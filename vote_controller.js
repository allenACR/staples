function init_grantr_plugin () {
  $.getScript( "https://static.gooddonegreat.com/dev/m/npo_search/js/insertScript.js", function( data, textStatus, jqxhr ) {});
}

angular.module('languageApp', ['ngStorage', 'angucomplete-alt', 'truncate'])
  .directive('bindHtmlUnsafe', function($compile) {
    return function($scope, $element, $attrs) {

      // compile html
      var compile = function(newHTML) {
        newHTML = "<span>" + newHTML + "</span>";
        newHTML = $compile(newHTML)($scope);
        $element.html('').append(newHTML);
      };

      // html name
      var htmlName = $attrs.bindHtmlUnsafe;

      // watch for changes; compile it if it changes
      $scope.$watch(htmlName, function(newHTML) {
        if (!newHTML) return;
        compile(newHTML);
      });

    };
  })

   // ------------ ROSTER CONTROLLER ------------
  .controller('rosterController', ['$scope', '$timeout',
    function($scope, $timeout) {
        $scope.loading = {
          roster: null,
        };
        $scope.page = {
          hasRoster: null,
          refreshLookup: null
        }



        $scope.init = function(){



              if (rollbaseJS.team != ''){
                // get rooster unique id and dates
                sql = "SELECT DISTINCT id, name, createdAt FROM vm_registration WHERE (status='Submitted' OR status='Confirmed') AND R6601212=" + rollbaseJS.teamID + " ORDER BY createdAt DESC ";
              //  console.log(sql);
              //  console.log(rollbaseJS.teamID);
                rbf_selectQuery(sql, 100000, function(data) {
                   
                  if (data.length > 0){
                    // convert date object to string
                    for (var i = 0; i < data.length; i++){
                      data[i][2] = data[i][2].toString();
                      console.log(data[i][2])
                    }

                    $timeout(function(){ 
                        $scope.page.hasRoster = true;
                        $scope.listOfRosters = data; 
                    })
                  }
                  else{
                     $scope.page.hasRoster = false;
                  }
                })

              }
              else{
                  $scope.page.hasRoster = false;
              }
        };

        $scope.updateTShirt = function(index){            
            rbf_setField('roster', $scope.rosterData[index].id, "requirestshirt", $scope.rosterData[index].tshirt, true)
        }

        $scope.deleteRecord = function(index){
              var r = confirm("Are you sure you want to remove this employee from the roster?");
              if (r == true) {
                  $('#roster_tbody tr').eq(index).hide();                     // removes from table
                  rbf_deleteRecord('roster', $scope.rosterData[index].id)  // removes from rollbase
              }                    
        }

        $scope.deleteFromTeam = function(name, index){
              var r = confirm("Are you sure you want to remove " + name + " from the team?");
              if (r == true) {
                  
                  sql = "SELECT id FROM mg_employee3 WHERE name='" + name + "'";
                  // start process
                  rbf_selectQuery(sql, 1, function(data) {
                    removeID = data[0][0]; 

                   
                    rbf_getRelatedIds('R8330850', rollbaseJS.teamID, function(a, b, data){
                       for (i = 0; i < data.length; i++){
                          if (removeID ==data[i]){
                            data.splice(i, 1)
                          }
                       }
                       //rbf_getFields('vm_team', rollbaseJS.teamID, 'R8330850', callback)
                       rbf_setField('vm_team', rollbaseJS.teamID, 'R8330850', data, true)

                       // hide in table
                       var hideString = "#tableContent tr:nth(" + index + ")"
                       $(hideString).hide()

                       // hide in rosterTable (if exists)
                       location.reload();


                     })

                    
                  })

              }     
        };


        $scope.lookupRoster = function(lookup){

                  $scope.loading.roster = true;


                  var packet = [
                    {fieldLabel: "requirestshirt",  tag: "tshirt" },
                    {fieldLabel: "id",  tag: "id" }
                  ]
                  var relatedFields = [
                    {field: "R12870837", grabField: "name", name: "tagName" }
                  ]                  
                  whereStatement = 'WHERE uniqueid="' + lookup  + '"'
                  
                  utilityjs.getFromRollbase("roster", packet, relatedFields, whereStatement, function(data){
                        console.log(data);
                        $scope.rosterData = []
                        for (var n = 0; n < data.nameList.length; n++){
                            
                             r  = data[data.nameList[n]]; 
                             name = r.data.tagName[0]; 
                             id   = r.id;
                             t  = r.tshirt;
                             if (t == 0|| t == null){ t = false}
                             else{ t = true};                             
                             $scope.rosterData.push({id: id, name: name, tshirt: t})
                                              
                        }

                        $timeout(function(){
                          $scope.loading.roster = false;
                        })       

                  })            
        }


    }
  ])



   // ------------ GALLERY ------------
  .controller('galleryController', ['$scope', '$timeout',
    function($scope, $timeout) {

        $scope.showLarge = false;
        $scope.showPreview = true;
        $scope.fullGalleryData = {};
        $scope.count = 0;

        $scope.init = function(){
          $timeout(function(){
            $scope.galleryObj = new Object();
            pullGalleryData();
          },100)
        }

        $scope.openLargeFull = function(data, index){
        
          $scope.currentSlide = index;
          $scope.maxSlides = $scope.currentGallery.length - 1;
          $scope.showLarge = true;
          $scope.largeImage = $scope.currentGallery[index].file;
          $scope.largeCaption = $scope.currentGallery[index].caption;

        };

        $scope.openLarge = function(data, key, index){
          $('body').css('overflow', 'hidden')
          $scope.setGallery(key)
          $scope.currentSlide = index;
          $scope.maxSlides = $scope.currentGallery.length - 1;
          $scope.showLarge = true;
          $scope.largeImage = $scope.currentGallery[index].file;
          $scope.largeCaption = $scope.currentGallery[index].caption;
        };

        $scope.next_currentGallery = function(){
          $scope.currentSlide++;
          if ($scope.currentSlide >= $scope.currentGallery.length){
            $scope.currentSlide = 0;
          }
          $scope.largeImage = $scope.currentGallery[$scope.currentSlide].file;
          $scope.largeCaption = $scope.currentGallery[$scope.currentSlide].caption;
        }

        $scope.prev_currentGallery = function(){
          $scope.currentSlide--;
          if ($scope.currentSlide < 0){
            $scope.currentSlide = $scope.currentGallery.length -1;
          }
          $scope.largeImage = $scope.currentGallery[$scope.currentSlide].file;
          $scope.largeCaption = $scope.currentGallery[$scope.currentSlide].caption;
        }

        $scope.closeLarge = function(){
          $timeout(function(){
            $scope.showLarge = false;
            $('body').css('overflow', 'auto')
          })
        };

        $scope.setGallery = function(key){
          $scope.currentGallery =  $scope.galleryObj[key];
        }

        $scope.showFullGallery = function(name, galleryData){
           $scope.currentGallery = galleryData;
           $scope.showPreview = false;
           $scope.galleryName = name;
           $scope.fullGalleryData = galleryData;
        };

        $scope.returnToPreview = function(){
          $scope.showPreview = true;
          $scope.fullGalleryData = {};
        }


        function pullGalleryData(){

            var integratedFields = [
            {field: "R12846481", grabField: "name", name: "name" }
            ]
            var standardFields = [

            {fieldLabel: "image_file",  tag: "file" },
            {fieldLabel: "caption",  tag: "caption" }
            ]
            var whereStatement = "WHERE approved = 1 "
            utilityjs.getFromRollbase("image_gallery", standardFields, integratedFields, whereStatement, function(data){
              
              // GET IMAGES TO CORRECT FOR ROLLBASE
              for (i = 0; i < data.nameList.length; i++){
                var org = data[data.nameList[i]];
                $.get(org.file)
              }
              //
              reorg(data);
            });
        }




        function reorg(object){


            for (var i = 0; i < object.nameList.length; i++){
              var obj = object[object.nameList[i]],
              name = obj.data.name[0];
              if($scope.galleryObj[name] == undefined){
                $scope.galleryObj[name] = [];
              }
              $scope.galleryObj[name].push({file: utilityjs.parseRollbaseImageString(obj.file),
                caption: obj.caption
              })
            }

            $scope.$apply();

        }

    }
   ])
   // ------------ END GALLERY -------------


  .controller('wrapperController', ['$scope', '$localStorage', '$sessionStorage', '$http', '$timeout',
    function($scope, $localStorage, $sessionStorage, $http, $timeout) {

      // set defaults
      $scope.languages = [{
        short: 'en',
        full: 'English'
      }, {
        short: 'esp',
        full: 'Spanish'
      }, {
        short: 'fr',
        full: 'French'
      }, {
        short: 'nl',
        full: "Dutch"
      }, {
        short: 'pt',
        full: "Portuguese"
      }, ];

      $scope.pathing = ""; // leave blank
      $scope.toggleLanguage = false;

      // localStorage / set default for language settings
      $scope.$storage = $localStorage.$default({
        languageSetting: null
      });
      $scope.selectedLang = $scope.$storage.languageSetting;

      // english storage for easy translation
      $scope.englishReference = null;

      //define variable for grantr plugin
      $scope.searchCity = "";

      // load any init code here
      $scope.init = function() {


        var current_visitor = window.current_visitor;
        if (current_visitor == null || current_visitor == undefined || current_visitor <= 0) {
          window.current_visitor = {
            id: null
          };
        }

        if ($scope.selectedLang == null) {
          $scope.loadSavedLanguage(function() {
            $timeout(function() {
              $scope.getJSONPacket(true, "en");
              $('html').css('opacity', 1);
            }, 100);
          });
        } else {
          $scope.getJSONPacket(true, "en");
          $('html').css('opacity', 1);
        }


      };

      $scope.loadSavedLanguage = function(callback) {
        var visitorId = window.current_visitor.id;
        if (visitorId == null || visitorId <= 0) {
          $scope.$storage.languageSetting = $scope.languages[0];
          $scope.selectedLang = $scope.languages[0];
          callback();
        } else {
          rbf_getFields('mg_employee3', visitorId, 'mg_language_selector', function(id, obj, data) {
            var rb_language = data.mg_language_selector;
            if (rb_language === 'null' || rb_language === null) {
              $timeout(function() {
                $scope.$storage.languageSetting = $scope.languages[0];
                $scope.selectedLang = $scope.languages[0];
                callback();
              }, 0);
            } else {

              $timeout(function() {
                i = $scope.languages.length;
                while (i--) {
                  if ($scope.languages[i].full == rb_language) {
                    $scope.$storage.languageSetting = $scope.languages[i];
                    $scope.selectedLang = $scope.languages[i];
                  }
                }
                callback();
              }, 0);

            }
          });
        }
      };

      // refresh angular hooks
      $scope.refresh = function() {
        // if digest in process, skips apply()
        if (!$scope.$$phase) {
          $scope.$apply();
        }
        if (!search_and_replace.get_url_parts().isPageSelector) {
          //replace_rb_datepicker_with_jquery_ui
        }
      };

      // setting language - updates all filters and date/time picker
      $scope.setLang = function(init) {
        var visitorId = window.current_visitor.id;
        $scope.toggleLanguage = false;

        // save language setting
        $scope.$storage.languageSetting = $scope.selectedLang;

        // switch languages for date picker
        if (!search_and_replace.get_url_parts().isPageSelector) {
          replace_rb_datepicker_with_jquery_ui();
          datepicker_language_select($scope.selectedLang.short);
        }

        // load and set language
        if (visitorId > 1) {
          rbf_setField('mg_employee3', visitorId, 'mg_language_selector', $scope.selectedLang.full, false);
        }
        $scope.getJSONPacket(false, $scope.selectedLang.short);
      };

      // run this code to get the currently selected language
      $scope.getCurrentLanguage = function() {
        return $scope.selectedLang.short;
      };

      // this is called when we need to append an angularhook after the page has been loaded
      $scope.refreshValue = function(content, appendLocation) {
        var element = $('body'),
          scope = element.scope,
          injector = element.injector(),
          compile = injector.get('$compile');
        compile(content)($scope).appendTo(appendLocation);
        return ($(appendLocation).text());
      };

      // add angularhooks post load
      $scope.injectAngular = function(content, appendLocation) {
        var element = $('body'),
          scope = element.scope,
          injector = element.injector(),
          compile = injector.get('$compile');
        compile(content)($scope).appendTo(appendLocation);

      };

      $scope.getEnglishReference = function() {
        return $scope.englishReference;
      };

      $scope.getPathing = function() {
        return $scope.pathing;
      };

      // get langauge type for page selector
      $scope.getJSONPacket = function(init, language) {

        var execute = true;

        //  if englishRefrence exists and initilization is called again
        if ($scope.englishReference != null && (language == "en" && init)) {
          execute = false;
          search_and_replace.initiate(init, "ref");
        }

        // does not reload english .json if it already exists
        if ($scope.englishReference != null && language == "en" && execute) {
          execute = false;

          // refresh existing stuff
          $scope.employeePortal = $scope.englishReference.employeePortal;
          $scope.suggestionBox = $scope.englishReference.suggestionBox;
          $scope.shared = $scope.englishReference.shared;

          // if digest in process, skips apply()

          $scope.refresh();

          // check for new content
          search_and_replace.initiate(init, "ref");

        }

        // otherwise, look up appropriate .json
        if (execute) {
          // retrieve local language packet in .json form
          $http({
            method: 'GET',
            url: search_and_replace.get_url_parts().hostname + "/localization/" + search_and_replace.get_url_parts().c + "/" + language + ".json"
          }).
          success(function(data, status, headers, config) {

            // set englishReference if it does not exist / eliminates flicker if code needs to be reran
            if ($scope.englishReference == null && language == "en") {
              $scope.englishReference = data;
            }

            // pull from .json -> stick into angularReference
            $scope.rootPath = data;
            $scope.pathing = "rootPath"; // must match previous variable name

            // pathing
            $scope.employeePortal = data.employeePortal;
            $scope.suggestionBox = data.suggestionBox;
            $scope.shared = data.shared;

            // being switch and replace
            search_and_replace.initiate(init, language);

          }).
          error(function(data, status, headers, config) {
            // console.log("Could not retrieve .json package.");
            search_and_replace.hideLoading();
          });


        }
      };

      $scope.selectLanguage = function() {
        $scope.toggleLanguage = $scope.toggleLanguage === false ? true : false;
      };

      // used in search and replace
      // returns section of json
      $scope.pullSection = function(target) {
        return eval("$scope." + target);
      };

      $scope.showAdvancedOpts = false;
      $scope.toggle_advanced_opts = function() {
        if ($scope.showAdvancedOpts) {
          $scope.showAdvancedOpts = false;
          $scope.searchCity = "";
        } else {
          $scope.showAdvancedOpts = true;
        }
      };

      $scope.org_callback = function(orgData) {



        if (!orgData) return false;

        var org = orgData.originalObject,
          id = $('#donation-id').html(),
          enterRecipInfo = $('[name="Enter Recipient Details"]');
          fgParams = {
            q: 'government_id:' + org.ein.replace('-', '') //strip ein of dash for first giving
          };

        $('#edit-btn').hide(); //hide edit button???
        $('.lb').remove(); //removes duplicate spans

        //set rollbase fields
        rbf_setField("mg_donation", id, "mg_recipient_ein", org.ein, false);
        $('#mg_recipient_ein').html(org.ein).val(org.ein).hide().after('<span class="lb">' + org.ein + '</span>');

        //commenting out because of conflict with trigger 6136406
        // rbf_setField("mg_donation", id, "mg_recipient_name", org.name, false);
        $('#mg_recipient_name').html(org.name).val(org.name).hide().after('<span class="lb">' + org.name + '</span>');

        rbf_setField("mg_donation", id, "mg_recipient_address_line_1", org.address, false);
        $("#mg_recipient_address_line_1").val(org.address).hide().after('<span class="lb">' + org.address + '</span>');

        rbf_setField("mg_donation", id, "mg_recipient_address_line_full", org.address, false);
        $("mg_recipient_address_line_full").val(org.address).hide().after('<span class="lb">' + org.address + '</span>');

        rbf_setField("mg_donation", id, "mg_recipient_address_full", org.address, false);
        $("#mg_recipient_address_full").val(org.address).hide().after('<span class="lb">' + org.address + '</span>');

        rbf_setField("mg_donation", id, "mg_recipient_city", org.city, false);
        $("#mg_recipient_city").val(org.city).hide().after('<span class="lb">' + org.city + '</span>');

        rbf_setField("mg_donation", id, "mg_recipient_region", org.state, false);
        $("#mg_recipient_region").val(org.state).hide().after('<span class="lb">' + org.state + '</span>');

        rbf_setField("mg_donation", id, "mg_recipient_zip_postal_code", org.zip_code, false);
        $("#mg_recipient_zip_postal_code").val(org.zip_code).hide().after('<span class="lb">' + org.zip_code + '</span>');

        if (org.country !== null) {
          rbf_setField("mg_donation", id, "mg_recipient_country", org.country, false);
          $("#mg_recipient_country").val(org.country).hide().after('<span class="lb">' + org.country + '</span>');
        }

        rbf_setField("mg_donation", id, "mg_recipient_category_code", org.ntee_code, false);
        $("#mg_recipient_category_code").val(org.ntee_code).hide().after('<span class="lb">' + org.ntee_code + '</span>');

        $('#mg_recipient_address_line_2')
          .add('#mg_recipient_address_line_3')
          .add('#mg_recipient_country')
          .add('#mg_recipient_url')
          .add('#mg_recipient_phone_number')
          .add('#mg_recipient_alias')
        .hide().after('<span class="lb"></span>');

        $('#rbi_L_mg_recipient_religious_organization')
          .add('#rbi_F_mg_recipient_religious_organization')
          .hide();

        $('[type=submit]').attr('disabled', false);

        //search First Giving for Org UUID
        $.ajax({
          dataType: 'jsonp',
          contentType: 'application/json',
          data: fgParams,
          jsonp: 'jsonpfunc',
          url: 'https://graphapi.firstgiving.com/v1/list/organization?jsonpfunc=?',
          success: function(data) {
            if (data.payload.length === 0) data.payload[0] = {};

            var fgOrg = data.payload[0];

            if (fgOrg.organization_uuid !== undefined) {
              rbf_setField("mg_donation", id, "mg_recipient_uuid", fgOrg.organization_uuid, false);
            }
          },
          error: function(error) {
            //TODO:  what to do in the event of a FG ajax error?
          }
        });

      };

      if (search_and_replace.get_url_parts().isPageSelector) {
        $scope.init();
      }
    }
  ]);
//
//*****************************************************//



//*****************************************************//
// automatically initiated when angular and DOM are ready
// this is slightly after jquery ready()
$('html').css('opacity', 0);

angular.element(document).ready(function() {
  var body = $('body'),
    // add loadingScreen and languageSelector
    spinnerScript = '<div class="blackground"></div><div id="loadingContainer" class="spinnerContainer"><i class="fa fa-cog fa-spin fa-2x"></i></div>',
    langSelector = "<div id='langSelector' class='no-print'><i class='icon-globe' style='cursor: pointer' ng-click='selectLanguage()'> &nbsp;{{ shared.shrd.sel_1 }}</i></div>",
    langPopup = "<div id='languagePopup' class='no-print' ng-class='{true: \"popup-show\", false: \"popup-hide\"}[toggleLanguage]'>",
    formHTML = $('form').prop('outerHTML');

  langPopup += "<br>{{shared.shrd.lsd}} &nbsp;&nbsp;<select style='padding-bottom: 10px' ng-model='selectedLang' ng-options='language.full for language in languages' ng-change='setLang()'><option value=''>{{ shared.shrd.shared.selects.vm_public_or_private_team.0 }}</option> </select>";
  langPopup += "<br><br></div>";

  // captcha, if applicable
  if ($("#capcha").length > 0) {
    $('#capcha').gdgCapcha();
  }

  // attach controller
  body.attr('languageApp', 'wrapperController');

  // append to body
  //body.append(spinnerScript + langSelector + langPopup);

  // check for grantr if appliable
  if (typeof grantr_plugin !== 'undefined') grantr_plugin.init();

  //IE 8 and 9 fixes for invalid RB HTML
  if (navigator.userAgent.indexOf("MSIE 9.0") >= 0 || navigator.userAgent.indexOf("MSIE 8.0") >= 0) {
    //fixes the login form
    var loginForm = $('[name=loginForm]');
    loginForm.replaceWith(loginForm.prop('outerHTML'));

    //fixes grid contols
    var gridRows = $('[name=gridRows_0]').parent();
    gridRows.replaceWith(gridRows.prop('outerHTML'));
  }

  // initialize angular app
     angular.bootstrap(document, ['languageApp']);
});
//
//*****************************************************//